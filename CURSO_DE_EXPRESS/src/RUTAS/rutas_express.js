import express from "express";
import { Router } from "express";

const app=express();

const rutas_inventario=Router();
const index_ruta=Router();

index_ruta.get("/", (rep,res)=>{
   res.send("ruta inicial");
});

rutas_inventario.get("/sobre",(rep,res)=>{
    res.send("ruta nosotros")
});
rutas_inventario.get("/productos",(rep,res)=>{
    res.send("ruta productos.");
});

app.use(rutas_inventario);
app.use(index_ruta);

app.listen(4000);
console.log("server en el puerto 4000")



