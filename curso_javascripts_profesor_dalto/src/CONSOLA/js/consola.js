// //console.log() mostrar alguna variable en consola
// console.log("hola mundo")

// //console.info() mostrar alguna informacion en consola
// console.info([1,2,3,4,5])

// //console.table() mostrar alguna informacion de alguna estructura de datos array u objetos
// console.table([1,2,3,4,5]);

// //console.error() mostrar algun error creado por nosotros mismos
// console.error("se produjo un error en el servidor");

// //console.clear() nos permite limpiar la console
// // console.clear();

// //con %c le estamos indicando que le agregaremos estilos al mensaje por console pero limitado a propiedades
// console.log("%c Francisco","color:red;border:2px solid blue;padding:30px;");

