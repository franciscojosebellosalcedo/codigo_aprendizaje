/**
 * 
 FORMAS DE OBTENCION DE UN ELEMENTO EN JAVASCRIPT


 */

// //document.querySelector : obtenemos un elemento por class o por id
// document.querySelector(".parrafo1");
// document.querySelector("#parrafo1");

// //document.querySelectorAll() : obtenemos una lista de elementos o de nodo (no es un array)
// document.querySelectorAll(".parrafo");

// //document.getElementById() : obtenemos un elemento por id
// document.getElementById("parrafo1")

// // //document.getElementsByClassName : obtenemos un elemento por class
// document.getElementsByClassName(".parrafo1")

// // document.getElementsByTagName() : obtenemos un elemento por nombre de etiqueta
// document.getElementsByTagName("p");

// // document.getElementsByName(): obtenemos un elemento por name
// console.log(document.getElementsByName("p1"))


/**
 DEFINIR , MODIFICAR Y ELIMINAR ATRIBUTOS DE UN ELEMENTO EN JAVASCRIPT



 */

// const p=document.querySelector("#parrafo_1");
// p.setAttribute("tabindex","1");
// console.log(p.getAttribute("tabindex"));
// p.removeAttribute("tabindex");


/**
 ATRUBUTOS GLOBALES DE UN ELEMENTO (LOS PUEDE RECIBIR CUALQUIER ELEMENTO HTML)



 */
//id : nos permite identificar a un unico elemento html
//class: aparte de los id nos permite identificar a un elemento html
//contenteditable: podemos editar el contenido de una elemento html
//hidden: establece si el elemento puede ser visible o no (oculta al elemento html)
//tabindex: precionando la tecla tab , definir que elemento se seleccionara primero
//style: podemos establece estios de css a un elemento html
//title: definir un titulo a un elemento html

/**
 ATRIBUTOS DIRECTOS DE UN INPUT


 */

// const input= document.querySelector(".input");
// input.type="file";
// input.className;
// input.accept="image/png";
// input.form="formulario"; : definimos de que formulario pertenece este input en caso de que este fuera de el
// input.placeholder="text";
// input.required="true";
// input.minLength="5";

// let titulo=document.querySelector("#clase");
// let listaClases=titulo.classList;
// console.table(listaClases);
// listaClases.remove("clase1")
// console.log(listaClases.contains("clase1"))
// console.log(listaClases.item(0));
// console.log(listaClases.replace("clase1","clase0"));

// let contenedor=document.querySelector(".contenedor");
// let p=document.createElement("P");
// let texto=document.createTextNode("hollla pedro");
// p.appendChild(texto);
// contenedor.appendChild(p);

