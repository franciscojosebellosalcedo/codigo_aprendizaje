// EVENTOS: Se asocia mediante un codigo  con un elemento de la pagina, como por ejemplo un boton.

// const btn=document.querySelector(".btn");

// btn.addEventListener("click",()=>saludar("fracisco"));

// function saludar(nombre) {
//   alert(`hola ${nombre}`);
// }

//OBJETO EVENTO: Este objeto es generado cuando se efectua una accion y es un objeto de suma importancia.

// const btn=document.querySelector(".btn");

// btn.addEventListener("click",(e)=>{
//   console.log(e);
//   console.log(e.target.name);
// });

//TIPOS DE EVENTOS DEL MOUSE

// 1: click

// const btn=document.querySelector(".btn");

// btn.addEventListener("click",()=>saludar("fracisco"));

// function saludar(nombre) {
//   alert(`hola ${nombre}`);
// }

// 2: dblclick

// const btn=document.querySelector(".btn");

// btn.addEventListener("dblclick",()=>saludar("fracisco"));

// function saludar(nombre) {
//   alert(`hola ${nombre}`);
// }

// 3: mouseover

// const btn=document.querySelector(".btn");

// btn.addEventListener("mouseover",()=>saludar("fracisco"));

// function saludar(nombre) {
//   alert(`hola ${nombre}`);
// }

// 4:mouseout

// const btn=document.querySelector(".btn");

// btn.addEventListener("mouseout",()=>saludar("fracisco"));

// function saludar(nombre) {
//   alert(`hola ${nombre}`);
// }

// 5: contextmenu: Es un tipo de evento del mouse que se ejecuta cuando presionamos click derecho del mouse

// const btn=document.querySelector(".btn");

// btn.addEventListener("contextmenu",()=>saludar("fracisco"));

// function saludar(nombre) {
//   alert(`hola ${nombre}`);
// }

// TIPOS DE EVENTOS DEL TECLADO

// 1: keypress: Este tipo de evento se ejecuta cuando se deja presionada alguna tecla

// const btn=document.querySelector(".btn");

// btn.addEventListener("keypress",(e)=>{
//   alert("hola pacho mira la consola");
//   console.log(e.key);
//   console.log(e);
// });

// 2: keydown: Este tipo de evento se ejecuta cuando se deja presionada alguna tecla

// const btn=document.querySelector(".btn");

// btn.addEventListener("keydown",(e)=>{
//   alert("hola pacho mira la consola");
//   console.log(e.key);
//   console.log(e);
// });

// TIPOS DE ENEVOTOS DE INTERFAZ (WINDOW)

// 1: load: Este tipo de evento se ejecuta una vez que haya cargado toda la ventana o pagina web

// window.addEventListener("load", () => {
//   console.log("Se cargo la pagina web");
//   const btn = document.querySelector(".btn");

//   btn.addEventListener("click", () => {
//     alert("hola pacho");
//   });
// });

// 2: error: Este tipo de evento se ejecuta cuando algun archivo de multimedia no carga correctamente
// 3: beforeunload: Este tipo de evento se ejecuta antes de irse a otro sitio mediante la etiqueta a (se ejecuta muy rapido)
// 4: unload: Este  tipo de evento se ejecuta cuando ya me fui a otro sitio web mediante la etiqueta a (se ejecuta muy rapido)
// 5: scroll: Este tipo de evento se ejecuta cada vez que demos scroll a la pagina web
// 6. select: Este tipo de esvento se ejecuta cuanoo seleccionamos en texto escrito en un input o textarea, cada vez que seleccionemos el texto se ejecutara




//stopPropagation: hace que el flujo de eventos se detenga

// const box1=document.querySelector(".box1");
// const box2=document.querySelector(".box2");
// const box2__btn=document.querySelector(".box2__btn");

// box1.addEventListener("click",(e)=>{
//   alert("hola soy box1");
//   console.log(e.target);
//   e.stopPropagation();
// });
// box2.addEventListener("click",(e)=>{
//   alert("hola soy box2");
//   console.log(e.target);
//   e.stopPropagation();
// });
// box2__btn.addEventListener("click",(e)=>{
//   alert("hola soy el boton de box2");
//   console.log(e.target);
//   e.stopPropagation();
// });








