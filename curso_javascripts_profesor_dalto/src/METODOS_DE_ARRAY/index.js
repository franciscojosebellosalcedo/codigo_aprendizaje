/**
 metodo pop(): saca o remueve el ultimo elemento de un array (retorna el elemento removido)
 */
// let numeros=[1,2,3,4,5];
// let elemento_removido=numeros.pop();
// console.log(elemento_removido);
// console.log(numeros);

/**
 metodo shift(): saca o remueve el primer elemento de un array (retorna el elemento removido)
 */
// let numeros=[1,2,3,4,5];
// let elemento_removido=numeros.shift();
// console.log(elemento_removido);
// console.log(numeros);

/**
 metodo push(datosNuevo): agrega a un elemento al final del array
 */
// let numeros=[1,2,3,4,5];
// numeros.push(6)
// console.log(numeros);

/**
 metodo reverse() : invierte el orden de un array
 */
// let numeros=[1,2,3,4,5];
// console.log(numeros.reverse());

/**
 metodo unshift(datoNuevo): agrega un nuevo elemento al principio del array
 */
// let numeros=[1,2,3,4,5];
// numeros.unshift(6)
// console.log(numeros);

/**
 metodo sort(): ordera el array
 */
// let numeros=[2,5,1,4,3];
// let nombres=["bernal","andres","cielo"]
// console.log(nombres.sort()); orden numerico
// console.log(numeros.sort()); orden alfabetico

/**
 metodo splice(indexInicial,cantidadAEliminar,datosNuevo): elimina una cantidad de elemento
 desde un indice indicando hasta donde termina y tambien permite agregar nuevos
 elementos
 */
// let numeros=[1,2,3,4,5];
// numeros.splice(1,2);
// console.log(numeros);

/**
 metodo join(datosString): convierte al array en  una cadena de texto y le podemos
 decir de que manera podemos separar los datos del array
 */
// let numeros=[1,2,3,4,5];
// let resultado=numeros.join("-");
// console.log(numeros);
// console.log(resultado);
// console.log(resultado[1]);

/**
 metodo slice(indexInicial,indexFinal): devuelve un array  de un array inicial 
 */
// let numeros=[1,2,3,4,5];
// console.log(numeros);
// console.log(numeros.slice(0,3));