/*
metodo concat(datosString) : concatena o une una cadena de texto con otra
*/ 
// let cadena="hola ";
// cadena=cadena.concat("francisco");
// console.log(cadena)
/*

metodo startWith(datosString): devuelve true si la cadena inicial empieza con el caracter o palabra  que le
 psasemos por parametro ,false en caso contrario
*/
// let cadena="hola ";
// console.log(cadena.startsWith("h"));
/*

metodo endWith(datosString): devuelve true si la cadena inicial termina con el caracter o palabra  que le
 psasemos por parametro ,false en caso contrario
*/
// let cadena="hola";
// console.log(cadena.endsWith("a"));

/*
 metodo includes(datosString): verifica si lo que le pasemos por parametro se encuentra en la cadena de texto (devuelve true o false)
 */
// let cadena="hola";
// console.log(cadena.includes("a"));

/**
 metodo indexOf(datosString): obtenemos el index del caracter  que le psasemos de la cadena inicial 
 devuelve -1 en caso de que no se encuentre
 */
// let cadena="hola";
// console.log(cadena.indexOf("a"));

/**
 metodo lastIndexOf(datosString): obtenemos el index del ultimo caracter de una cadena de texto
devuelve -1 en caso de que no se encuentre
 */
// let cadena="hola";
// console.log(cadena.lastIndexOf("l"));

/**
 metodo padStar(cantidad,datosString): rellena la cadena al principo con los caracteres deseados
 pero indicando el tamaño de la cadena para poder pasar mas caracteres
 */
// let cadena="hola";
// console.log(cadena.padStart(6,"12"));
/**
 
 metodo padEnd(cantidad,datosString): rellena la cadena al final con los caracteres deseados
 pero indicando el tamaño de la cadena para poder pasar mas caracteres
 */
// let cadena="hola";
// console.log(cadena.padEnd(6,"12"));

/**
 metodo repeat(datoEntero): repite la cadena nuevamente cuantas queramos
 */
// let cadena="hola ";
// console.log(cadena.repeat(2));

/**
 metodo split(datoString): divide una cadena como 
 queramos y devuelve un array
 */
// let cadena="hola.como.estas";
// console.log(cadena.split("."));

/**
 metodo substring(indexPrincipal,indexFinal): nos devuelve
 un estring o una cadena de texto desde donde le indiquemos
 */
// let cadena="ABCDEFGHI";
// console.log(cadena.substring(0,4));

/**
 metodo toLowerCase(): convierte a una cadena en minuscula
 */
// let cadena="ABCDEFGHI";
// console.log(cadena.toLowerCase());

/**
 metodo toUpperCase(): convierte a una cadena en mayuscula
 */
// let cadena="abcdfghi";
// console.log(cadena.toUpperCase());

/*
metodo toString(): convierte un dato en string
*/
// let numeros=[1,2,3,4,5];
// numeros=numeros.toString();
// console.log(typeof(numeros));

/**
 metodo trim(): elimina los espacio del inicio y fin
 de la cadena de texto
 */
// let cadena="  hola  ";
// console.log(cadena.length);
// console.log(cadena.trim().length);

/**
 metodo trimStart(): elimina los espacio de las cadenas al inicio
 */
// let cadena="  hola";
// console.log(cadena.length);
// console.log(cadena.trimStart().length);
/**


 metodo trimEnd(): elimina los espacio de las cadenas al final
 */
// let cadena="hola  ";
// console.log(cadena.length);
// console.log(cadena.trimEnd().length);

