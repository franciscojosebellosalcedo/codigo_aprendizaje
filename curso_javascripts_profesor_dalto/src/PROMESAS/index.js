
// UTLIZANDO PROMESA COMUN

// const promesa = new Promise((resolve, reject) => {
//     const peticion = fetch('https://dummyjson.com/products/');
//     if (peticion === null) {
//         reject("Se produjo un error");
//     } else {
//         resolve(peticion);
//     }
// });
// promesa
//     .then(res => {
//         return res.json();
//     })
//     .then(pro => {
//         mostrarEnConsola(pro);
//     })


// UTILIZANDO ASYNC /AWAIT

// function enviarPeticion(url) {
//     return fetch(url)
// }
// async function obtenerDatos() {
//     try {
//         const response = await enviarPeticion('https://dummyjson.com/products/')
//         const data = await response.json();
//         console.log(data);
//         mostrarEnLista(data);
//     } catch (error) {

//     }
// }
// obtenerDatos();

// function mostrarEnLista(data) {
//     const ul = document.querySelector(".ul");
//     const fragment = document.createDocumentFragment();
//     const { products } = data;
//     for (let index = 0; index < products.length; index++) {
//         const producto = products[index];
//         const li = document.createElement("LI");
//         li.innerHTML = ` ${producto.title}`;
//         fragment.appendChild(li);
//     }
//     ul.appendChild(fragment);
//     agregarEvento()
// }

// function agregarEvento() {
//     const listaLi = [...document.querySelectorAll("li")];
//     listaLi.map((li) => {
//         li.style.cursor = "pointer";
//         li.style.border = "1px solid black";
//         li.style.width = "max-content";
//         li.style.marginBottom = "2px";
//         li.addEventListener("click", (e) => {
//             e.preventDefault();
//             alert("soy un celular")
//         });
//     })
// }



