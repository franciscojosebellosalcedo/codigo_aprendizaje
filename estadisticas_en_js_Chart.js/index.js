
const printChart = () => {
    const elemento_1 = document.querySelector(".grafica__1");

    const xValues = ["Colombia", "France", "Spain", "USA", "Venezuela", ""];
    const yValues = [90, 100, 44, 30, 1.7, 0];
    const barColors = ["green", "blue", "red", "orange", "black", ""];
    const data = {
        labels: xValues,
        datasets: [{
            label: "hola",
            data: yValues,
            backgroundColor: barColors,

        }],
    }
    new Chart(elemento_1, { type: "bar", data });



    window.onscroll = function () { funcionScroll() };

    function funcionScroll() {
        let fija = document.querySelector(".fija");
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            fija.classList.add("mostrar");
            fija.addEventListener("click",()=>subirScroll());
        } else {
            fija.classList.remove("mostrar");
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function subirScroll() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

}
printChart();



