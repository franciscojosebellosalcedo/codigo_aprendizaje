document.addEventListener("DOMContentLoaded",()=>{
    const to_top=document.querySelector(".to_top");
    const listaH1=[...document.querySelectorAll(".titulo")];
    window.addEventListener("scroll",()=>{
        let desplazo=window.scrollY;
        if(desplazo>500){
            to_top.classList.add("activo");
        }else{
            to_top.classList.remove("activo");
        }
        let altura=window.innerHeight/5*4;
        console.log(altura);
        listaH1.map(h1=>{
            if(h1.getBoundingClientRect().top<altura){
                h1.classList.add("ver");
            }else{
                h1.classList.remove("ver");

            }
        });
    });
    to_top.addEventListener("click",()=>{
        window.scrollTo(0,0);
    });
});